from internetarchive import upload
from sys import argv

script, episode = argv

# Create variables based on the episode input to use during upload
flac = "/home/thaj/Music/" + episode + ".flac"
mp3 = "/home/thaj/Music/" + episode + ".mp3"
ogg = "/home/thaj/Music/" + episode + ".ogg"

# Create the description for the metadata
episodename = input("What is the name of the episode? Or if Thaj Rambles a synopsis. ")

# Create the tags for the episode
tags = input("Create tags, serperated by ; ")

# A dictionary to hold all of the metadata to be uploaded
metadata = dict(collection='opensource_audio', identifier=episode, title=episode, mediatype='audio', description=episodename, creator='Thaj',licenseurl='https://creativecommons.org/licenses/by-sa/4.0/', language='eng', subject=tags)

# Upload the files and metadata to Archive.org
upit = upload(episode, files=[flac, mp3, ogg], metadata=metadata)
upit[0].status_code

