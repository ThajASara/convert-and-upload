#!/bin/bash


# Scripts to convert files to multople formats

ffmpeg -i $1.flac -ar 44100 -b:a 192k $1.mp3
ffmpeg -i $1.flac -ar 44100 -b:a 96k $1.ogg

# Print out size of the mp3 and ogg files
echo "-----------------------------------"
echo "{$1}.mp3 size is: "
ls -l $1.mp3 | awk '{print $5}'
echo "-----------------------------------"
echo "{$1}.ogg size is: "
ls -l $1.ogg | awk '{print $5}'
echo "-----------------------------------"
